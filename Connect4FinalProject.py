import os
import time
##What to fix: Fix custom array in Punishprint
##Fix blade option select!
##this is a test
def printConnect(): ##prints in rows 
    for row in connect4:
        # Loop over columns.
        for column in row:
            print("|" + str(column), end="|")
        print(end="\n")
    print("---------------------")
    return None
def punishPrint(player, pType,punishP1T1,punishP1T2,punishP2T1,punishP2T2): ##added in 2.0
    if (player == 1):
        if (pType == 1):
            print("Cooldown: " + str(punishP1T1) + " turns!")
        else:
            print("[" + str(punishP1T2[0]) + "," + str(punishP1T2[1]) + "," + \
                str(punishP1T2[2]) + "," + str(punishP1T2[3]) + "]") ##printing out array custom

    if (player == 2):

        if (pType == 1):
            print("Cooldown: " + str(punishP2T1) + " turns!")
        else:
            print("[" + str(punishP2T2[0]) + "," + str(punishP2T2[1]) + "," + \
                str(punishP2T2[2]) + "," + str(punishP2T2[3]) + "]") ##printing out array custom

def plop(player, nameP1, nameP2): ##changes board Piece by droping
    test = False
    row = 5 
    ##printConnect()
    if (player == 1):
        col = input("Enter what col (1-7): " + str(nameP1) + "#: ") ##gets input for what collum
    else:
        col = input("Enter what col (1-7): " + str(nameP2) + "#: ") ##gets input for what collum
    col = int(col)
    while (test == False):
        if (col != ""):
            col -= 1 ##sets the index back
            if (col >= 0 and col <= 6): ##checks to see if index out of range!
                test = True ##exits while
                continue
        else:
            col = input("Enter what col (1-7): Player " + str(player) + "#: ") ##if null asks input again
    test = False
    while (test == False): ##loops till value at point is 0
        if (connect4[row][col] == 0):##checks if value is zero
            connect4[row][col] = player ##sets current value to player number
            test = True ##returns true to show input went through
        elif (row < 0): ##if y (x) is already full
            print("Row if full, try again!")
            return False ##returns false to show input did not go through
        else:
            row -= 1 ##decrements row to go up on the array 
    print("\n")
    printConnect()

    return True 
 
def checkWinV(player): ##checks for verticle win NEEDS TO BE FIXED
    player1count = 0 ##test value for player 1
    player2count = 0 ##test value for player 2
    
    col = 0

    while (col <= 6): ##loops through each collum POSSIBLE CHANGE TO FOR LOOP
        prevous = 0 ##prevous number
        collum = list() ##creats list
        
        for row in range(6): ##loops through collum 
            collum.append(connect4[row][col]) ##appends value to list
        collum.reverse() ## reverses it to set it in order
        for i in range(6): ## row takes all values in list (indexed)
            value = collum[i] ##sets a test value (used for debuging)

            if (value == 1): ##adds count value if is player 1 piece
                if (prevous == 2): ##if is other player
                    player1count = 1 ##sets value to one (somewhat reset)
                    prevous = value ##sets prevous value to current
                else: 
                   player1count += 1 ##adds value to one 
                   prevous = value ##sets prevous value to current
            elif(value == 2): ##adds count value if is player 2 piece
                if(prevous == 1): ##checks for other value in prevous
                    player2count = 1 ##sets value to one (somewhat reset
                    prevous = value ##sets prevous value to current
                else: 
                   player2count += 1 ##adds value to player 2
                   prevous = value ##sets prevous value to current
            else:
                player1count = 0 ##resets count so it does not keep the original count
                player2count = 0 ## ^
                prevous = value ##sets prevous value to current

            if (player == 1): ##checks player value
                if (player1count >= 4): ##if value >= 4 then sets true
                    return True ##exits command
            elif (player == 2):##checks player value
                if (player2count >= 4):##if value >= 4 then sets true 
                    return True ##exits command

        col += 1 ##increments to change collum
        
        
    return False ##exits command

def checkWinH(player): ##checks for horisontal win
    player1count = 0 ##player 1 test value
    player2count = 0 ##player 2 test value

    prevous = 0 ##prevous value
    
    for row in connect4: ##gets the first row
        # Loop over columns.
        for column in row: ##takes index value of row
            
            value = column ##sets prevous value

            if (value == 1): ##adds count value if is player 1 piece
                if (prevous == 2): ##checks for opisite value
                    player1count = 1 ##sets value to 1 (somewhat reset)
                    prevous = value ##updates prevous value
                else: 
                   player1count += 1 ##adds 1 to player 1
                   prevous = value ##updates prevous value
            elif(value == 2): ##adds count value if is player 2 piece
                if (prevous == 1): ##checks for opisite value
                    player2count = 1 ##sets value to 1 (somewhat reset)
                    prevous = value ##updates prevous value
                else: 
                   player2count += 1 ##adds 1 to player 2
                   prevous = value  ##updates prevous value
            else:
                player1count = 0 ##resets count so it does not keep the original count
                player2count = 0 ## ^
                prevous = value ##updates prevous value

            if (player == 1): ##checks player value
                if (player1count >= 4): ##if value >= 4 then sets true
                    return True ##exits command
            elif (player == 2):##checks player value
                if (player2count >= 4):##if value >= 4 then sets true 
                    return True ##exits command
         
        
    return False ##if no match just returns false

def checkWinDf(player): ##diagnal forwards
    
    player1count = 0 ##player 1 test value
    player2count = 0 ##player 2 test value

    col = 0 ##sets to first col
    row = 5 ##sets to last row
    countRow = 0 ##updates row value
    countCol = 0 ##updates col value

    prevous = 0 ##defines prevous
    value = 0 ##defines value
    newRow = 5 ##starts and defines new row to bottom
    trow = False ##boolean loop value
    while (row >= 3): ##loops till row is greater than 3
            i = 0 ##Count value
            trow = False ##on loop resets value to false
            while (trow != True): ##loops till row ends
                
                value = connect4[row][col] ##sets current value

                if (value == 1): ##adds or sets count value if is player 1 piece
                    if (prevous == 2): ##checks if opisite number
                        player1count = 1 ##sets count to 1 (somewhat reset)
                        prevous = value ##updates prevous value
                    else: 
                        player1count += 1 ##adds 1 to count
                        prevous = value ##updates prevous value
                elif(value == 2): ##adds count value if is player 2 piece
                    if (prevous == 1): ##checks for opisite number
                        player2count = 1 ##sets count to 1 (somewhat reset)
                        prevous = value ##updates prevous
                    else: 
                        player2count += 1 ##adds 1 to count
                        prevous = value ##updates prevous
                else:
                    player1count = 0 ##resets count so it does not keep the original count
                    player2count = 0 ## ^
                    prevous = value ##updates prevous

                if (player == 1): ##checks player value
                    if (player1count >= 4): ##if value >= 4 then sets true
                        return True ##exits command
                elif (player == 2):##checks player value
                    if (player2count >= 4):##if value >= 4 then sets true 
                        return True ##exits command
                col += 1 ##col moves up one
                row -= 1 ##row moves to the prevous row
                i += 1 ## count goes up
                if (i >= 6 or row < 0 or col > 6): ##checks to move on (with exception handleing)
                    countCol += 1 ##adds 1 to count col
                    col = 0 + countCol ##start col moves up 1
                    row = newRow ##sets row back to default
                    if (col > 6): ##when collum goes over the index enters if
                        countRow += 1 ##subracts 1 to move row up one
                        col = 0 ##col is reset to 0
                        newRow = 5 - countRow ##new row changes value
                        row = newRow ##sets row to newRow
                        countCol = 0 ##resets count col
                        trow = True ##exits while
                    
    return False
  



def checkWinDb(player): ##diagnal backwords       
    player1count = 0 ##player1 test value
    player2count = 0 ##player2 test value

    col = 6 ##sets end of col
    row = 5 ##set last row
    countRow = 0 ##modifies row
    countCol = 0 ##modifies col

    prevous = 0 ##defines prevous
    value = 0 ##defines value
    newRow = 5 ##sets newRow to last row
    trow = False ##boolean exit test
    while (row >= 3): ##loops till is greaterthan 3
            i = 0 ##sets count
            trow = False ##resets value on loop
            while (trow != True): ##goes till row is done
                
                value = connect4[row][col] ##sets current value

                if (value == 1): ##adds count value if is player 1 piece
                    if (prevous == 2): ##checks for prevous value
                        player1count = 1 ##sets count to 1 (somewhat reset)
                        prevous = value ##updates prevous
                    else: 
                        player1count += 1 ##adds 1 to count
                        prevous = value ##updates prevous
                elif(value == 2): ##adds count value if is player 2 piece
                    if (prevous == 1): ##checks for other value
                        player2count = 1 ##sets count to 1 (somewhat reset)
                        prevous = value ##updates prevous
                    else: 
                        player2count += 1 ##adds 1 to count 
                        prevous = value ##updates prevous
                else:
                    player1count = 0 ##resets count so it does not keep the original count
                    player2count = 0 ## ^
                    prevous = value ##updates prevous

                if (player == 1): ##checks player value
                    if (player1count >= 4): ##if value >= 4 then sets true
                        return True ##exits command
                elif (player == 2):##checks player value
                    if (player2count >= 4):##if value >= 4 then sets true 
                        return True ##exits command
                col -= 1 ##goes back one collum
                row -= 1 ##goes back one row
                i += 1 ##Loops goes up one
                if (i >= 6 or row < 0 or col < 0): ##checks for nest col (with exception handling
                    countCol += 1 ##adds one to count col
                    col = 5 - countCol ##sets starting collum back 1
                    row = newRow ##sets row back to default row
                    if (col < 0): ##checks to go to next row
                        countRow += 1 ##count row adds 1
                        col = 6 ##collum is reset to 6
                        newRow = 5 - countRow ##newRow has new default
                        row = newRow ##changes to new default
                        countCol = 0 ##resets countCol
                        trow = True ##exits loop
    return False ##exits to not be true

def stab():
    col = int(input("Enter the col you want to poke!")) + 1
    row = 0
    while(row < 6):
        connect4[row][col] = 0
        row += 1

def poke():
    row = int(input("Enter the row you want to poke!")) + 1
    col = 0
    while(col < 7):
        connect4[row][col] = 0
        col += 1

    update(row)

def update(row):
    col = 0

    if (row == 6):
        return

    while (col < 7):
        connect4[row][col] = connect4[row+1][col]
        connect4[row+1][col] = 0
        col += 1


            

def reset():

    ##List sets an example of a reset board
    list = [[int(0) for col in range(7)],[int(0) for col in range(7)],
            [int(0) for col in range(7)],[int(0) for col in range(7)],
            [int(0) for col in range(7)],[int(0) for col in range(7)]]


    return list #returns the value and sets it to the variable the comand uses    

##assignments
p1 = 1 ##Marker for Player 1
p2 = 2 ##Marker for Player 2
playerCurrent = p1 ##current players turn 
gameP1 = False ##Player1 Win
gameP2 = False ##Player2 Win

def playerswitch(player):
    if (player == 1): ##if player is player 1
        plr = 2 ##switchs to player 2
    elif (player == 2): ##if player is player 2
        plr = 1 ##switchs to player 1
    return plr ##returns value

connect4 = [[int(0) for col in range(7)],[int(0) for col in range(7)],
            [int(0) for col in range(7)],[int(0) for col in range(7)],
            [int(0) for col in range(7)],[int(0) for col in range(7)]] ##connect 4 board
        
done = False ##while loop value 1
doneD = False ##while loop value 2
player1name = "Player 1"
player2name = "Player 2"

while(done != True): ##game loop
    connect4 = reset() ##resets board
    print("Welcome to connect 4:  \n") ##welcomes player
    
    answer = input("1. Play game (simple), 2. play (standard), 3. debug, 4. Shortcut/secrets 5.quit ")
     ##gets input on what player wants ^^

    if (answer == "names"):
        player1name = input("Enter player 1's name:    ")
        player2name = input("Enter player 2's name:    ")
        if (player1name == ""):
            player1name = "Player 1"
        if (player2name == ""):
            player2name = "Player 2"


        answer = 4
    answer = int(answer)
    if (answer == 1): ##starts game
        os.system('cls') ##clears screen
        playerCurrent = p1 ##sets playercurrent to player 1
        game = True ##game loop value
        turn = 1 ##sets first turn
        while (game == True): ##game loop
            print("Turn " + str(turn) + ":  \n") ##prints turn

            if (turn == 1): ##shows current board on turn 1
                printConnect() 

            switch = plop(playerCurrent,player1name, player2name) ##plops value and checks if row is not full

            while (switch != True): ##if switch is false (row full) then ask player again
                switch = plop(playerCurrent,player1name, player2name) ##asks input again

            ##checks all win conditons
            if ((checkWinDf(playerCurrent) == True) or (checkWinDb(playerCurrent) == True) or \
                (checkWinH(playerCurrent) == True) or (checkWinV(playerCurrent) == True)): 
                print("Player " + str(playerCurrent) + " has won!") ##prints win message
                game = False ##exits loop
                break ##exits loop
            playerCurrent = playerswitch(playerCurrent) ##switch player

            switch = plop(playerCurrent,player1name, player2name) ##plops value and checks if row is not full

            while (switch != True): ##if switch is false (row full) then ask player again
                switch = plop(playerCurrent,player1name, player2name) ##asks input again

            ##checks all win conditons
            if ((checkWinDf(playerCurrent) == True) or (checkWinDb(playerCurrent) == True) or \
                (checkWinH(playerCurrent) == True) or (checkWinV(playerCurrent) == True)):
                print("Player " + str(playerCurrent) + " has won!") ##prints win condition
                game = False ##sets game loop off
                break ##exits loop

            turn += 1 ##adds 1 to value

            playerCurrent = playerswitch(playerCurrent) ##switches player
    elif (answer == 2): ##New in v2.0 Standard
        os.system('cls') ##clears screen
        playerCurrent = p1 ##sets playercurrent to player 1
        set = True ##set loop value
        game = True #game loop value
        turn = 1 ##sets first turn
        winsP1 = 0 ##how many P1 wins 
        winsP2 = 0 ##how many P2 wins 
        winner = 0 ##who is the winner
        round = 1 ##currentRound
        punishType = 1 ##what punish type is there.
        rounds = False
        currentSetType = 0 ##currentWin count
        BEST_OF_3 = 2 ##constant B03
        BEST_OF_5 = 3 ##ConstantB05
        STR_BO3 = " best of three "
        STR_BO5 = " best of five "
        maxRounds = 0

        while (rounds != True): ##tests for true input.
            maxRounds = int(input("1. Best of three 2. Best of 5:   ")) ##Settings for set length
            maxRounds = int(maxRounds)
            if (maxRounds == 1 or maxRounds == 2):
                if (maxRounds == 1):
                    currentSetType = BEST_OF_3
                elif (maxRounds == 2):
                    currentSetType = BEST_OF_5
                break


        while (set == True): ##set loop

            if (winsP1 == currentSetType or winsP2 == currentSetType): ##checks for a set win
                if (currentSetType == 2):
                    if (playerCurrent == 1):
                        print(str(player1name) + " has won the best of" + STR_BO3 + "set!") ##prints winner BO3
                    if (playerCurrent == 2):
                        print(str(player2name) + " has won the best of" + STR_BO3 + "set!") ##prints winner BO3

                elif (currentSetType == 3):
                    if (playerCurrent == 1):
                        print(str(player1name) + " has won the best of" + STR_BO5 + "set!") ##prints winner BO3
                    if (playerCurrent == 2):
                        print(str(player2name) + " has won the best of" + STR_BO5 + "set!") ##prints winner BO3

                break ##breaks out of set loop

            punTypeBol = False
            while (punTypeBol != True):
                punTypeInput = int(input("1. Cooldown 2.Limited usage:  "))
                if (punTypeInput == 1 or punTypeInput == 2): ##used for changing mid game
                    if (punTypeInput == 1):
                        punishType = 1 
                        break ##breaks out of choice loop
                    else:
                        punishType = 2
                        break ##breaks out of choice loop

            print("Round: " + str(round))

            playerCurrent = 1
            
            punishP1T1 = 0 ##player1 type 1 punish
            punishP2T1 = 0 ##player2 type 1 punish

            punishP1T2 = [1,1,1,1] ##player1 type 2 punish
            punishP2T2 = [2,2,2,2] ##player2 type 2 punish

            punishP1T2Current = 3
            punishP2T2Current = 3

            while (game == True):
                print(player1name + " " + str(winsP1) + "|" + str(winsP2) + " " + player2name)
                print("Turn " + str(turn) + ":  \n") ##prints turn

                if (turn == 1): ##shows current board on turn 1
                    printConnect() 

                punishPrint(playerCurrent, punishType, punishP1T1, punishP1T2, punishP2T1, punishP2T2) ##prints pumish status

                avaiable = False           
                while (avaiable == False):
                    blade = int(input("Would you like to: 1. Stab, 2. Poke, 3. skip:   "))
                    if (punishP1T1 > 0):
                        print("Stab is not available!")
                    elif (punishP1T2[0] == 0):
                        print("Poke is not available!")
                    else:
                        avaiable = True
                if (blade == 3):
                    switch = plop(playerCurrent,player1name, player2name) ##plops value and checks if row is not full

                    while (switch != True): ##if switch is false (row full) then ask player again
                        switch = plop(playerCurrent,player1name, player2name) ##asks input again
                elif(blade == 2):
                    poke()
                    if (playerCurrent == 1):
                        if (punishType == 1):
                            punishP1T1 += 2
                        elif (punishType == 2):
                            punishP1T2.pop()
                    if (playerCurrent == 2):
                        if (punishType == 1):
                            punishP2T1 += 2
                        elif (punishType == 2):
                            punishP2T2.pop()
                elif (blade == 1):
                    stab()
                    if (playerCurrent == 1):
                        if (punishType == 1):
                            punishP1T1 = 2
                        elif (punishType == 2):
                            if (punishP1T2Current != 0 or punishP2T2Current <= 3):
                                punishP1T2[punishP1T2Current] == 0
                                punishP2T2[punishP2T2Current] == 2
                                punishP1T2Current -= 1
                                punishP2T2Curruent += 1

                    if (playerCurrent == 2):
                        if (punishType == 1):
                            punishP2T1 = 2
                        elif (punishType == 2):
                            punishP2T2[punishP2T2Current] == 0
                            punishP1T2[punishP1T2Current] == 1
                            punishP2T2Current -= 1
                            punishP1T2Curruent += 1

                ##checks all win conditons
                if ((checkWinDf(playerCurrent) == True) or (checkWinDb(playerCurrent) == True) or \
                    (checkWinH(playerCurrent) == True) or (checkWinV(playerCurrent) == True)): 
                    print("Player " + str(playerCurrent) + " has won!") ##prints win message\
                    ###checks for winning player###
                    if (playerCurrent == 1): 
                        winsP1 += 1 ##updates player 1's win count
                    elif (playerCurrent == 2):
                        winsP2 += 1 ##updates player 2's win count
                    break ##exits loop
                playerCurrent = playerswitch(playerCurrent) ##switch player

                punishPrint(playerCurrent, punishType, punishP1T1, punishP1T2, punishP2T1, punishP2T2) ##prints pumish status

                avaiable = False ##sets avaiable to false
                while (avaiable == False):
                    blade = int(input("Would you like to: 1. Stab, 2. Poke, 3. skip:   "))
                    if (punishP2T1 > 0 or len(punishP2T2) == 0):
                        print("Option is not use able!")
                    else:
                        avaiable = True
                if (blade == 3):
                    switch = plop(playerCurrent,player1name, player2name) ##plops value and checks if row is not full

                    while (switch != True): ##if switch is false (row full) then ask player again
                        switch = plop(playerCurrent,player1name, player2name) ##asks input again
                elif(blade == 2 and punishP2T1 == 0 or len(punishP2T2) != 0):
                    poke()
                    if (playerCurrent == 1):
                        if (punishType == 1):
                            punishP1T1 = 2
                        elif (punishType == 2):
                            punishP1T2.pop()
                    if (playerCurrent == 2):
                        if (punishType == 1):
                            punishP2T1 += 2
                        elif (punishType == 2):
                            punishP2T2.pop()
                elif (blade == 1):
                    stab()
                    if (playerCurrent == 1):
                        if (punishType == 1):
                            punishP1T1 = 2
                        elif (punishType == 2):
                            punishP1T2.pop()
                    if (playerCurrent == 2):
                        if (punishType == 1):
                            punishP2T1 = 2
                        elif (punishType == 2):
                            punishP2T2.pop()     
                ##checks all win conditons
                if ((checkWinDf(playerCurrent) == True) or (checkWinDb(playerCurrent) == True) or \
                    (checkWinH(playerCurrent) == True) or (checkWinV(playerCurrent) == True)):
                    print("Player " + str(playerCurrent) + " wins round " + str(round)) ##prints winner of round

                    ###checks for winning player###
                    if (playerCurrent == 1): 
                        winsP1 += 1 ##updates player 1's win count
                    elif (playerCurrent == 2):
                        winsP2 += 1 ##updates player 2's win count
                    break ##exits loop

                turn += 1 ##adds 1 to value
                if (punishType == 1):
                    if (punishP1T1 != 0):
                        punishP1T1 -= 1
                    if (punishP2T1 != 0):
                        punishP2T1 -= 1

                playerCurrent = playerswitch(playerCurrent) ##switches player
            connect4 = reset()
            time.sleep(2)
            os.system('cls')
            turn = 1
            round += 1

    elif (answer == 3): ##enters debug
        while (doneD != True):
            ans = input("1. plop, 2. checkWinH, 3. checkwinV, 4. checkwinD, 5. switchplayer, " + \
                "6. printConnect, 7. Reset #: (c to clear scene)") ##gets input on what command
            if (ans == "c"): ##clears the scene
                    os.system('cls')
                    continue ##continues
            if (ans == "c" or ans == "'" or ans == " "): ##gets input
                ans = input("1. plop, 2. checkWinH, 3. checkwinV, 4. checkwinD, " + \
                    "5. switchplayer, 6. printConnect, 7. Reset #: ") ##asks what command again
                
                
            else:
                ans = int(ans) ##changes value to int
                
            if (ans == 1): ##executes plop
                plop(playerCurrent,player1name, player2name)
            elif (ans == 2): ##exicutes horizontal win
                bol = checkWinH(playerCurrent)
                print(bol)
            elif (ans == 3): ##exucutes verticle
                bol = checkWinV(playerCurrent)
                print(bol)
            elif (ans == 4): ##executes both daignal
                bol = checkWinDf(playerCurrent) ##forwards
                bol2 = checkWinDb(playerCurrent) ##backwords
                print("Forwards: " + str(bol))
                print("Backwords: " + str(bol2))
            elif (ans == 5): ##switches player
                playerCurrent = playerswitch(playerCurrent) 
            elif (ans == 6): ##prints board
                printConnect()
            ##elif (ans == 7): ##resets board
                ##stab(1)
            elif (ans == 7): ##resets board
                connect4 = reset()
            elif (ans == 8): ##Tests stab
                stab()
            elif (ans == 9):
                poke()
            else:
                break
    elif (answer == 4):
        print("q: quit | names: change of names | more soon")
    elif (answer == 5): ##exits game
        done = True
    

